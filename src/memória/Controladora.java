package memória;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */
public class Controladora {
    int[] memoria;
    int tamanhoMemoriaReal, tamanhoDoSO, qtdeDeProcessos;
    int enderecoBase;
    //List<Processo> processos = new ArrayList();
    List<Processo> filaDeEspera = new ArrayList();
    
    Controladora(int tamanhoMemoriaReal,
                 int tamanhoDoSO,
                 int qtdeDeProcessos
                ){
        memoria = new int[tamanhoMemoriaReal];
        this.tamanhoMemoriaReal = tamanhoMemoriaReal;
        this.tamanhoDoSO = tamanhoDoSO;
        this.qtdeDeProcessos = qtdeDeProcessos;
        for(int i=0; i<tamanhoDoSO; i++) memoria[i]= -1;
        for(int i=tamanhoDoSO; i<tamanhoMemoriaReal; i++) memoria[i]= 0;
        
        /* *isto fica aqui ou na interface*********************
        for(int i=0; i<qtdeDeProcessos; i++){
            Processo processo = new Processo();
        }
        ******************************************************/
    }
    
    int procuraLacuna(){
        int tamanhoDaLacuna = 0;
        
        //este "for" pesquisa o endereço base da próxima lacuna
        for(;(memoria[enderecoBase]!=0) && (enderecoBase<tamanhoMemoriaReal); enderecoBase++);
        
        //se tiver chegado ao fim da memória o próximo "for" não terá efeito
        
        //este "for" mede o tamanho dessa lacuna
        for(;(memoria[enderecoBase]==0) && (enderecoBase<tamanhoMemoriaReal); enderecoBase++)
            tamanhoDaLacuna++;
        
        //o "endereço base" foi incrementado uma vez a mais no código acima
        enderecoBase--;
        
        return tamanhoDaLacuna;
        
    }
    
    void alocaNaMemoria(int tamanhoDoProcesso){
        int enderecoLimiteDoProcesso = enderecoBase + tamanhoDoProcesso;
        
        //aqui altera-se o vetor "memoria"
        //for(int i=enderecoBase; i<enderecoLimiteDoProcesso; i++) memoria[i]= Interface..id;
        
        //continuar daqui
    }
    
    void alocaNaFila(){
        
    }
    
    void firstFit(int tamanhoDoProcesso){
        //"lacunaAtual" guarda o tamanho da lacuna encontrada a cada verificação do método "procuraLacuna"
        //por causa do "for" a seguir, atribuímos antecipadamente um valor a ele
        int lacunaAtual = -1;
        
        //define-se aqui o endereço inicial da pesquisa na memória de processos
        enderecoBase = tamanhoDoSO;
        
        //este "for" descrobre a próxima lacuna e "lacunaAtual" recebe o tamanho da lacuna.
        //Se não houver nenhuma lacuna que caiba o processo, "lacunaAtual" recebe ZERO.
        for(; (lacunaAtual<tamanhoDoProcesso) && (lacunaAtual!=0); ) lacunaAtual = procuraLacuna();
        
        //decide se aloca na memória ou na fila de espera
        if(lacunaAtual==0) alocaNaFila();
        else alocaNaMemoria(tamanhoDoProcesso);
        
        //alocaProcesso();
        
    }
    
    void bestFit(){
        
    }
    
    void worstFit(){
        
    }
    
    void nextFit(){
        
    }
}
